package Fruite_Market;

import Fruite_Market.Mediator.Separator;
import Fruite_Market.Model.Transaction;
import Fruite_Market.Shop.Fruit;
import Fruite_Market.Shop.FruitFactory;
import Fruite_Market.Shop.Ledger;

import java.io.IOException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(System.in);
        Log fruit_log = new Log("fruit_log.txt");

        while (true) {
            String input = scanner.nextLine();
            String[] operation = input.split(" ");
            Separator command;
            if (operation.length > 4) {
                System.out.println("INPUT IS NOT VALID (OPERATION FRUIT_NAME PRICE QUANTITY)");
                fruit_log.logger.warning("INVALID INPUT:- " + input);
                continue;
            }
            try {
                command = new Separator(operation);
            } catch (Exception exception) {
                System.out.println("INPUT IS NOT VALID (OPERATION FRUIT_NAME PRICE QUANTITY)");
                fruit_log.logger.warning("INVALID INPUT:- " + input);
                continue;
            }
            switch (command.getOperation()) {
                case BUY -> {
                    Fruit fruitObj = FruitFactory.getFruit(command.getFruitName());
                    fruitObj.buy(new Transaction(command.getPrice(), command.getQuantity()));
                    String transaction_Info = "BUY " + command.getQuantity() + " KG " + command.getFruitName() + " AT " + command.getPrice() + " RUPEES/KG";
                    //System.out.println(transaction_Info);
                    fruit_log.logger.info(transaction_Info);
                }
                case SELL -> {
                    Fruit fruitName = FruitFactory.getFruit(command.getFruitName());
                    fruitName.sell(new Transaction((command.getPrice()), command.getQuantity()));
                    String transaction_Info = "SOLD " + command.getQuantity() + " KG " + command.getFruitName() + " AT " + command.getPrice() + " RUPEES/KG";
                    fruit_log.logger.info(transaction_Info);
                }
                case PROFIT -> {
                    String profit_Info = "Profit is :- " + Ledger.getProfit();
                    fruit_log.logger.info(profit_Info);
                    System.out.println(profit_Info);
                }
                case QUIT -> {
                    return;
                }
                default -> throw new IllegalArgumentException("INPUT FORMAT IS NOT VALID");
            }
        }
    }
}
