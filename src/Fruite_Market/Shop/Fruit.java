package Fruite_Market.Shop;

import Fruite_Market.Model.Transaction;

import java.util.*;

public class Fruit {
    //for LIFO use stack
    //private Stack<Transaction> fruitList = new Stack<>();

    //for optimal approach
    //private PriorityQueue<Transaction> fruitList = new PriorityQueue<>();

    //for FIFO use List
    private List<Transaction> fruitList = new ArrayList<>();
    public int quantity;

    public Fruit() {
        quantity = 0;
    }

    public void buy(Transaction purchase) {
        fruitList.add(purchase);
        quantity += purchase.quantity;
    }

    public void sell(Transaction selling) {
        Iterator iterator = fruitList.iterator();
        int sellWeight = selling.quantity;
        if (quantity < sellWeight)
            throw new IllegalArgumentException("Not Available");
        quantity -= sellWeight;
        int cost = 0;
        while (iterator.hasNext()) {
            Transaction purchase = (Transaction) iterator.next();
            if (purchase.quantity >= sellWeight) {
                int remaining = purchase.quantity - sellWeight;
                cost += sellWeight * purchase.price;
                fruitList.set(0, new Transaction(selling.price, remaining));
                break;
            }
            sellWeight -= purchase.quantity;
            cost += purchase.quantity * purchase.price;
            iterator.remove();
        }
        Ledger.setProfit(Ledger.getProfit() + (selling.quantity * selling.price - cost));
    }
}
