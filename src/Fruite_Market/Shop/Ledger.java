package Fruite_Market.Shop;

public class Ledger {
    private static int profit;

    public static int getProfit() {
        return profit;
    }

    public static void setProfit(int profit) {
        Ledger.profit = profit;
    }
}
